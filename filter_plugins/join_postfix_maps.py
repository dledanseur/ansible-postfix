# vim: set expandtab

def join_postfix_maps(data):
    '''Join every attribute of the object in the data list witj the attr_separator
    and join the resulting string with the lst_separator'''

    res = []
    
    for m in data:
        str = ''
        for a in m["maps"]:
            append = ''
            type = a["type"]
            
            if type == 'tcp':
                append = 'tcp:'+a["address"]
            elif type == 'variable':
                append = a["value"]
            elif type == 'unix':
                append = 'unix:'+a["value"]
            else:
                filename = m["name"]
                if "filename" in a:
                    filename = a["filename"]
                append = type+':/etc/postfix/'+filename+'_map.cf'
            
            if str == '':
                str = append
            else:
                str += ', '+append 
        res.append({"name": m["name"], "value": str})
    return res
            

class FilterModule(object):

    def filters(self):
        return {
            'join_postfix_maps': join_postfix_maps
        }

if __name__ == "__main__":
    data=[{

        "name": "lst1",
	"maps": [
            {
	        "type": "mysql",
                "filename": "test"
	    },
            {
                "type": "hash",
                "filename": "test2"
            },
            {
                "type": "unix",
                "value": "my.prog"
            },
            {
                "type": "variable",
                "value": "$var"
            }
        ]},
        {
	    "name": "lst2",
            "maps": [
                {
                    "type": "regexp"
                }
            ]
        },
        {      
            "name": "lst3", 
            "maps": [{
                "type": "tcp",
                "address": "127.0.0.1:1234"
            }]
        }
        
    ]

    res = join_postfix_maps(data)
    assert len(res) == 3
    assert res[0]["name"] == 'lst1'
    assert res[0]["value"] == 'mysql:/etc/postfix/test_map.cf, hash:/etc/postfix/test2_map.cf, unix:my.prog, $var'
    assert res[1]["name"] == 'lst2'
    assert res[1]["value"] == 'regexp:/etc/postfix/lst2_map.cf'
    assert res[2]["name"] == 'lst3'
    assert res[2]["value"] == 'tcp:127.0.0.1:1234'

 
